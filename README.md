# Guerre_des_moutons_jux_et_kevine

## Présentation de la simulation
On va modéliser le fonctionnement d'un écosystème, avec de l'herbe, des moutons dans un premier temps, puis des loups dans un deuxième temps et enfin des chasseurs dans un dernier temps.

Cette simulation est discrète : tous les « tours de jeu » (tous les ticks d'horloge), les moutons ainsi que les loups se déplacent, mangent des moutons ou meurent éventuellement, et l'herbe pousse, les chasseurs tue des loups..etc..

Notre objectif est de simuler cet écosystème en utilisant essentiellement un paradigme

de programmation Objet.

Ce projet sera réalisé à partir d'un éditeur Python (VS code, Thonny, IDLE...)

## Les developpeurs

- ZENIA Junior TG5
- RAFIDISON Kévine TG6
