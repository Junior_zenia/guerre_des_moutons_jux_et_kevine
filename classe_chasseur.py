from random import *


class Chasseur:
    
    def __init__(self, position_x, position_y):
        self.position_x = position_x
        self.position_y = position_y
        
    def deplacement(self,tableau): #fait deplaer le chasseur de façon aléatoire
        dx = randint(-1, 1)
        dy = randint(-1, 1)
        self.position_x = (self.position_x + dx) % tableau.dimension
        self.position_y = (self.position_y + dy) % tableau.dimension
        return self.position_x, self.position_y

    def adjacentes_chasseur(self,loup,monde): #fait deplacer le chasseur de façon intelligente
        if self.position_x == 0 and self.position_y == 0: # coin en haut à gauche
            if (self.position_x, self.position_y + 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            elif (self.position_x + 1, self.position_y + 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            elif (self.position_x + 1, self.position_y + 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
        if self.position_x == 0 and self.position_y == monde.dimension - 1: # coin en haut à droite
            if (self.position_x - 1, self.position_y - 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (self.position_x - 1, self.position_y - 1)
            elif (self.position_x + 1, self.position_y + 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (self.position_x + 1, self.position_y + 1)
            elif (self.position_x + 1, self.position_y - 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
        if self.position_x == monde.dimension - 1 and self.position_y == 0: #coin en bas à gauche
            if (self.position_x + 1, self.position_y ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x + 1, self.position_y + 1 ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x , self.position_y + 1 ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
        if self.position_x == monde.dimension - 1 and self.position_y == monde.dimension - 1:
            if (self.position_x , self.position_y -1) == (loup.position_x, loup.position_y):# coin en bas à droite
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x - 1, self.position_y - 1 ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x -1 , self.position_y) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
        if self.position_x == 0 and self.position_y != 0 and self.position_y != monde.dimension - 1: #le chasseur sur une case du haut hormis 0,0 et 0,dimension
            if (self.position_x , self.position_y -1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x + 1, self.position_y - 1 ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x + 1, self.position_y) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x + 1, self.position_y + 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x, self.position_y + 1 ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
        if self.position_x == monde.dimension - 1 and self.position_y != 0 and self.position_y != monde.dimension - 1: #le chasseur sur une case du bas  hormis dimension,0 et dismension,dimension
            if (self.position_x , self.position_y -1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x, self.position_y + 1 ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x - 1, self.position_y - 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x - 1, self.position_y) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x - 1, self.position_y + 1 ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
        if self.position_y == 0 and self.position_x != 0 and self.position_x != monde.dimension - 1: #le chasseur sur une case à gauche  hormis dimension,0 et 0,0
            if (self.position_x - 1, self.position_y) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x - 1, self.position_y + 1 ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x, self.position_y + 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x + 1, self.position_y + 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x + 1, self.position_y) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
        if self.position_y == monde.dimension and self.position_x != 0 and self.position_x != monde.dimension - 1: #le chasseur sur une case à droite  hormis 0,dimension et dimension,dimension
            if (self.position_x - 1, self.position_y) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x - 1, self.position_y - 1 ) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x, self.position_y - 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x + 1, self.position_y - 1) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            if (self.position_x + 1, self.position_y) == (loup.position_x, loup.position_y):
                (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
            else:
                if (self.position_x - 1, self.position_y - 1) == (loup.position_x, loup.position_y): # si le chasseur se trouve sur les autres cases
                    (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
                if (self.position_x - 1, self.position_y) == (loup.position_x, loup.position_y):
                    (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
                if (self.position_x - 1, self.position_y + 1) == (loup.position_x, loup.position_y):
                    (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
                if (self.position_x , self.position_y - 1) == (loup.position_x, loup.position_y):
                    (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
                if (self.position_x , self.position_y + 1) == (loup.position_x, loup.position_y):
                    (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
                if (self.position_x + 1, self.position_y - 1) == (loup.position_x, loup.position_y):
                    (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
                if (self.position_x + 1, self.position_y ) == (loup.position_x, loup.position_y):
                    (self.position_x, self.position_y) = (loup.position_x, loup.position_y)
                if (self.position_x + 1, self.position_y + 1) == (loup.position_x, loup.position_y):
                    (self.position_x , self.position_y) = (loup.position_x, loup.position_y)
        else:
            self.deplacement() #s'il n'y pas de loup sur les cases adjacentes
    
    def placeChasseur(self, i, j):
        self.position_x = i
        self.position_y = j

    