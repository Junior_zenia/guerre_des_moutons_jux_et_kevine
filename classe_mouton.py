from random import *
from classe_monde import *


class Mouton:
    def __init__(self, position_x, position_y):
        self.gain_nourriture = 4
        self.position_x = position_x
        self.position_y = position_y
        self.energie = randint(1,2*self.gain_nourriture)
        self.taux_reproduction = 4
        a = randint(0,100)
        if a <= 50:
            self.sexe = 0 # mâle
        else:
            self.sexe = 1 # femelle
        
    def variationEnergie(self,tableau): # fait varie l'énergie du mouton s'il se trouve sur case avec de l'herbe
        if not tableau.is_empty(self.position_x,self.position_y):
            tableau.herbeMangee(self.position_x,self.position_y)
            self.energie += self.gain_nourriture
        else:
            self.energie -= 1
        return self.energie

    def deplacement(self,tableau): # fait deplacer le mouton aléatoirement
        dx = randint(-1, 1)
        dy = randint(-1, 1)
        self.position_x = (self.position_x + dx) % tableau.dimension
        self.position_y = (self.position_y + dy) % tableau.dimension
        return self.position_x, self.position_y

    def placeMouton(self, i, j): # place un mouton à une position i;j
        self.position_x = i
        self.position_y = j