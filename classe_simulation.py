from random import *
from classe_monde import *
from classe_mouton import*
from classe_lou import *
from classe_chasseur import *
import matplotlib.pyplot as plt
import numpy as nb 

class Simulation:
    def __init__(self, nombre_moutons, nombre_lou, nombre_chasseur, fin_du_monde, monde, max_moutons):
        self.nombre_moutons = nombre_moutons
        self.nombre_lou = nombre_lou
        self.nombre_chasseur = nombre_chasseur
        self.horloge = 0
        self.fin_du_monde = fin_du_monde
        self.mouton = []
        for i in range(nombre_moutons):
            self.mouton.append(Mouton(randint(0,monde.dimension-1),randint(0,monde.dimension-1)))
        self.lou = []
        for i in range(nombre_lou):
            self.lou.append(Lou(randint(0,monde.dimension-1),randint(0,monde.dimension-1)))
        self.chasseur = []
        for i in range(nombre_chasseur):
            self.chasseur.append(Chasseur(randint(0,monde.dimension-1),randint(0,monde.dimension-1)))
        self.monde = monde
        self.resultats_herbe = []
        self.resultats_moutons = []
        self.resultats_lou = []
        self.resultats_chasseur = []
        self.max_moutons = max_moutons
        
    def horloge_1(self): # fait augmenter l'horloge de 1
        self.horloge += 1

    def herbepousse(self): # fait pousser l'herbe dans le monde
        self.monde.herbePousse()
        
    def vie_mouton(self): # tue ou laisse en vie les moutons selon leurs énergies
        for i in self.mouton:
            i.variationEnergie(self.monde)
            if i.energie== 0:
                self.mouton.remove(i)
                
    def vie_lou(self): # tue un loups si son énergie = 0
        for i in self.lou:
            if i.energie == 0:
                self.lou.remove(i)
                  
    def reproduction(self):# fait reproduire les moutons selon leur taux de reproduction
        l = len(self.mouton)
        for i in range(l):
            x = randint(0,100)
            if x <= 4:
                self.mouton.append(Mouton(self.mouton[i].position_x,self.mouton[i].position_y))
                

    def reproduction_lou(self): # fait reproduire les moutons selon leur taux de reproduction
        l = len(self.lou)
        for i in range(l):
            x = randint(0,100)
            if x <= 5:
                self.lou.append(Lou(self.lou[i].position_x,self.lou[i].position_y))


    def deplace(self): # fait deplacer les moutons
        for i in self.mouton:
            i.deplacement(self.monde)
            
    def deplace_lou(self): # fait deplaer les loups
        for j in self.lou:
            for i in self.mouton:
                a = randint(0,100)
                if a <= 10:
                    j.adjacentes_lou(i,self.monde)
                else:
                    j.deplacement_lou(self.monde)
    
    def deplace_chasseur(self): # fait deplacer les chasseurs
        for j in self.chasseur:
            for i in self.lou:
                j.adjacentes_chasseur(i,self.monde)
    
    def resultat(self): # enregistre le nombre d'herbe , de mouton et de loups après chaque tours
        self.resultats_herbe.append(self.monde.nbHerbe())
        self.resultats_moutons.append(len(self.mouton))
        self.resultats_lou.append (len(self.lou))
        self.resultats_chasseur.append(len(self.chasseur))
    
    def mouton_manger(self): # fait que les loups mangent les moutons 
        for j in self.lou:
            for i in self.mouton:
                if i.position_x ==  j.position_x and i.position_y == j.position_y:
                    self.mouton.remove(i)
                    j.energie += j.gain_nourriture
                else:
                    j.energie -= 1
                    
    def tue_lou(self): # fait que les chasseurs mangent les loups
        for j in self.chasseur:
            for i in self.lou:
                if i.position_x ==  j.position_x and i.position_y == j.position_y:
                    a = randint(0,100)
                    if a <= 70:
                        self.lou.remove(i)
                  
    def sim(self):
        while self.max_moutons > len(self.mouton) and self.horloge < self.fin_du_monde:
            self.horloge_1()
            self.herbepousse()
            self.vie_mouton()
            self.reproduction()
            self.reproduction_lou()
            self.deplace_lou()
            self.deplace()
            self.tue_lou()
            self.deplace_chasseur()
            self.mouton_manger()
            self.vie_lou()
            self.resultat()
        return self.resultats_herbe , self.resultats_moutons, self.resultats_lou, self.resultats_chasseur
    
    def graphe(self):# affiche le graphe
        x = [i for i in range (self.fin_du_monde)]
        y1= nb.array(self.resultats_herbe)
        y2 = nb.array(self.resultats_moutons)
        y3 = nb.array(self.resultats_lou)
        plt.plot(y1,label='herbes')
        plt.plot(y2,label='moutons')
        plt.plot(y3,label='loup')
        plt.legend()
        plt.show()
        return ()
        
    


            
    