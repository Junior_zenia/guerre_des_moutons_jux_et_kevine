# Guerre_des_moutons_jux_et_kevine


## Classe Monde

### Attributs de la classe monde
- **dimension**: entier, représente la taille du côté
- **duree_repousse**: entier, vitesse de repousse de l'herbe
- **carte**: liste de liste... Matrice dimension X dimension

### Méthode de la classe monde
- **herbePousse**: fait repousser l'herbe dans les cases de ma matrice monde
- **is_empty**: prédicat pour savoir s'il y a de l'herbe sur la case (i;j)
- **herbeMangee(i,j)**: mange l'herbe sur la case (i;j) 
- **nbHerbe**: Retourne le nombre de case qui contiennent de l'herbe
- **getCoefCarte**: où i et j sont les indices du coefficient de ma matrice carte. C'est un accesseur de type particulier qui renvoie la valeur du coefficient i,j de la carte

## Classe Mouton

### Attributs de la classe Mouton

- **gain nourriture** : le gain d'énergie apporté par la consommation d'un carré d'herbe (on peut mettre 4 comme valeur par défaut)
- **position x et position y** : couple x,y qui représente la position du mouton.
- **energie** : entier positif (ou nul). Quand l'énergie d'un mouton est à 0, il meurt et l'objet est supprimé. Sera initialisé entre 1 et 2 × **gain nourriture**.
- **taux reproduction** : entier compris entre 1 et 100 (on peut mettre 4). Ce taux représente le pourcentage de chance qu'un mouton se reproduise
- **sexe** : entier compris entre 0 et 1 qui détermine le sexe du mouton. 0 pour mâle et 1 pour femelle.
### Méthodes de la classe Mouton

- **variation Energie (i,j)** : diminue de 1 l'énergie du mouton s'il n'est pas sur un carré d'herbe, sinon augmente de **gain nourriture**. Renvoie **energie**.
Remarques:
 si plusieurs moutons sont sur la même case herbue, seul le premier bénéficie du gain d'énergie 
 
- **deplacement** : le mouton se déplace aléatoirement dans une des huit cases adjacentes. Il est plus facile de considérer le monde comme torique (comme dans les jeux vidéo, sortir par le haut de la carte renvoie en bas etc.), on utilise alors l'opérateur modulo (%). On peut accepter le fait que le mouton ne se déplace pas à tous les coups
- **place mouton (i,j)** : place un mouton aux coordonnées (i, j), peut être utilisé lors de des naissances.

## Classe Simulation

### Attributs de la classe Simulation

- **nombre_moutons** : Nombre de moutons initialement présents sur la carte
- **horloge** : entier, initialisé à 0
- **fin_du_monde** : entier, donnant le temps maximum de la simulation
- **moutons** :  Liste d'objets Moutons
- **lou** :  Liste d'objets Lou
- **chasseur** :  Liste d'objets Chasseur
- **Monde**: Une instance de la classe Monde
- **resultats herbe**: Liste construite au fur et à mesure, donnant le nombre de case contenant de l'herbe
- **resultats moutons** : idem que la précédente mais pour les moutons.
- **resultats lou** : idem que la précédente mais pour les loups.
- **resultats chasseur** : idem que la précédente mais pour les chasseurs.
- **nombre_lou** : Nombre de loups initialement présents sur la carte
- **nombre_chasseur** : Nombre de chasseurs initialement présents sur la carte
- **max moutons** : nombre max de moutons que peut atteindre la simulation
#### Méthode unique, que l'on peut décomposer en plusieurs fonctions:
 
### Méthodes de la classe Simulation

- **simMouton**: gère la simulation en créant une boucle qui
    - **horloge_1** augmente Horloge de 1 à chaque appel;
    - **herbepousse** fait pousser l'herbe de monde;
    - **vie_mouton** appelle variationEnergie pour chaque mouton. Si l'énergie d'un mouton est nulle, l'instance est supprimée (un « remove » dans la liste des moutons)
    -**vie_lou**verifie pour chaque lou si son énergie = 0 si oui on le retire de la liste des loups
    -**reproduction** Fait se reproduire les moutons. Un nouveau mouton apparaît sur la même case que son parent, avec un pourcentage de naissance donné par **taux_reproduction**
    -**reproduction_lou** Fait se reproduire les loups. Un nouveau loup apparaît sur la même case que son parent, avec un pourcentage de naissance donné par **taux_reproduction**
    - **deplace**Fait se déplacer les moutons.
    - **deplace_lou**Fait se déplacer les loups et il y a 10 % de chance que le loups se déplace de mainère intelligentes(expliquer dans la classe Lou)
    - **deplace_chasseur**Fait se déplacer les chasseur   de manière intelligentes(expliquer dans la classe Chasseur)
    - **resultat** Sauvegarde dans **resultats_herbe** et **resultats_moutons** les nombres de carrés herbus et de moutons du tour.
    -**mouton_manger** représente le fait qu'un loup mange un mouton s'ils sont sur la même case.
    -**tue_lou** représente le fait qu'un chasseur tue un loup s'ils sont sur la même case et le chasseur à 70 % de reussir son coup.
    -**sim**
    - On peut arrêter la simulation s'il n'y a plus de moutons, ou s'il y en a plus qu'un nombre fixé (qu'on rajoutera en attribut). Dans ce dernier cas, les moutons ont conquis le monde..
    - On l'arrête dans tous les cas lorsque l'horloge sonne la fin du monde.
    - Cette fonction renvoie les deux listes de résultats.
    -**graphe** affiche les graphes représentant l'évolution de l'herbe, des moutons et des loups en fonction du nombre de coups.

## Classe Lou

### Attributs de la classe Lou
- **gain nourriture** : le gain d'énergie apporté par la consommation d'un carré d'herbe (on peut mettre 18 comme valeur par défaut)
- **position x et position y** : couple x,y qui représente la position du loups.
- **energie** : entier positif (ou nul). Quand l'énergie d'un loup est à 0, il meurt et l'objet est supprimé. Sera initialisé entre 1 et 2 × **gain nourriture**.
- **taux reproduction** : entier compris entre 1 et 100 (on peut mettre 5). Ce taux représente le pourcentage de chance qu'un loup se reproduise
- **sexe** : entier compris entre 1 et 0 qui détermine le sexe du loups. 0 pour mâle et 1 pour femelle.

### Méthodes de la classe Lou

- **deplacement** : le loup se déplace aléatoirement dans une des huit cases adjacentes. Il est plus facile de considérer le monde comme torique (comme dans les jeux vidéo, sortir par le haut de la carte renvoie en bas etc.), on utilise alors l'opérateur modulo (%). On peut accepter le fait que le loup ne se déplace pas à tous les coups
- **adjacentes_lou** : fais déplacer les moutosns de manière intelligentes ce qui veut dire qu'il ira vers un loup si ce dernier ce trouve sur une case adjacentes au loup.
- **placeLou (i,j)** : place un loup aux coordonnées (i, j), peut être utilisé lors de des naissances.

### Attributs de la classe Chasseur
- **position x et position y** : couple x,y qui représente la position du chasseur.
### Méthodes de la classe Chasseur

- **deplacement** : le chasseur se déplace aléatoirement dans une des huit cases adjacentes. Il est plus facile de considérer le monde comme torique (comme dans les jeux vidéo, sortir par le haut de la carte renvoie en bas etc.), on utilise alors l'opérateur modulo (%). On peut accepter le fait que le loup ne se déplace pas à tous les coups
- **adjacentes_chasseur** : fais déplacer les chasseur de manière intelligentes ce qui veut dire qu'il ira vers un loup si ce dernier ce trouve sur une case adjacentes au chasseur.
- **placeChasseur (i,j)** : place un chasseur aux coordonnées (i, j).



